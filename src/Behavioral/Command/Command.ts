interface Command {
    doing(): void;
}

class SimpleCommand implements Command {
    private payload: string;

    constructor(payload: string) {
        this.payload = payload;
    }

    public doing(): void {
        console.log(`SimpleCommand: See, I can do simple things like printing (${this.payload})`);
    }
}

class ComplexCommand implements Command {
    private receiver: Receiver;

    private a: string;
    private b: string;


    constructor(receiver: Receiver, a: string, b: string) {
        this.receiver = receiver;
        this.a = a;
        this.b = b;
    }

    public doing(): void {
        this.receiver.doSomething(this.a);
        this.receiver.doSomethingElse(this.b);
    }
}

//Получатель
class Receiver {
    public doSomething(a: string): void {
        console.log(`Receiver: Working on (${a}.)`);
    }

    public doSomethingElse(b: string): void {
        console.log(`Receiver: Also working on (${b}.)`);
    }
}

//Отправитель***
class Invoker {
    private onStart: Command;
    private onFinish: Command;

    public setOnStart(command: Command): void {
        this.onStart = command;
    }

    public setOnFinish(command: Command): void {
        this.onFinish = command;
    }

    public doSomethingImportant(): void {
        if (this.isCommand(this.onStart)) {
            this.onStart.doing();
        }
        if (this.isCommand(this.onFinish)) {
            this.onFinish.doing();
        }
    }

    private isCommand(object): object is Command {
        return object.doing !== undefined;
    }
}


const invoker = new Invoker();
const receiver = new Receiver();

invoker.setOnStart(new SimpleCommand('Say Hi!'));
invoker.setOnFinish(new ComplexCommand(receiver, 'Send email', 'Save report'));

invoker.doSomethingImportant();

invoker.setOnStart(new SimpleCommand('fffffffffffffff!'));
invoker.setOnFinish(new ComplexCommand(receiver, '1111', '222222'));

invoker.doSomethingImportant();