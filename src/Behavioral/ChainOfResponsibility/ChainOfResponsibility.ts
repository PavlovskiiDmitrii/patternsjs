abstract class AbstractHandler
{
    private nextHandler: AbstractHandler;

    public setNext(handler: AbstractHandler): AbstractHandler {
        this.nextHandler = handler;
        return handler;
    }

    public handle(request: string): string {
        if (this.nextHandler) {
            return this.nextHandler.handle(request);
        }
        return null;
    }
}


class MonkeyHandler extends AbstractHandler {
    public handle(request: string): string {
        if (request === 'Banana') {
            return `Monkey: eat ${request}.`;
        }
        return super.handle(request);
    }
}
class SquirrelHandler extends AbstractHandler {
    public handle(request: string): string {
        if (request === 'Nut') {
            return `Squirrel: eat ${request}.`;
        }
        return super.handle(request);
    }
}
class DogHandler extends AbstractHandler {
    public handle(request: string): string {
        if (request === 'MeatBall') {
            return `Dog: eat ${request}.`;
        }
        return super.handle(request);
    }
}

function ChainWorker(handler: AbstractHandler) {
    const foods = ['Banana', 'Nut', 'MeatBall', 'Bread'];

    for (const food of foods) {
        console.log(`${food} ?`);

        const result = handler.handle(food);
        if (result) {
            console.log(`${result}`);
        } else {
            console.log(`untouched. --------`);
        }
        console.log(` `);
    }
}

const monkey = new MonkeyHandler();
const squirrel = new SquirrelHandler();
const dog = new DogHandler();

monkey.setNext(squirrel).setNext(dog);

console.log('Chain: Monkey > Squirrel > Dog');
console.log('______________________________\n');
ChainWorker(monkey);

console.log('Subchain: squirrel >  Dog');
console.log('______________________________\n');
ChainWorker(squirrel);