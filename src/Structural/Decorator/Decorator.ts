interface IHuman {
    operation(): string;
}
class ConcreteHuman implements IHuman {
    public operation(): string {
        return 'DIMA';
    }
}
class ConcreteHumanMasha implements IHuman {
    public operation(): string {
        return 'Masha';
    }
}


class Decorator implements IHuman {
    protected component: IHuman;

    constructor(component: IHuman) {
        this.component = component;
    }

    public operation(): string {
        return this.component.operation();
    }
}
class SHAPKA extends Decorator {
    public operation(): string {
        return `SHAPKA(${super.operation()})`;
    }
}
class KURTKA extends Decorator {
    public operation(): string {
        return `KURTKA(${super.operation()})`;
    }
}


function clientCode(component: IHuman) {
    console.log(`RESULT --------- ${component.operation()}`);
}

console.log('new ConcreteHuman()');
clientCode(new ConcreteHuman());
console.log('');
console.log('new KURTKA(new SHAPKA(new ConcreteHuman())');
clientCode(new KURTKA(new SHAPKA(new ConcreteHuman())));
console.log('');
console.log('new KURTKA(new SHAPKA(new ConcreteHumanMasha())');
clientCode(new KURTKA(new ConcreteHumanMasha()));