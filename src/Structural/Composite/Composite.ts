abstract class Component {
    protected parent: Component;

    public setParent(parent: Component) {
        this.parent = parent;
    }

    public getParent(): Component {
        return this.parent;
    }

    public addComponent(component: Component): void { }

    public removeComponent(component: Component): void { }

    public isNested(): boolean {
        return false;
    }

    public abstract operation(): string;
}

class Product extends Component {
    public operation(): string {
        return 'Product';
    }
}

class Box extends Component {
    protected children: Component[] = [];

    public addComponent(component: Component): void {
        this.children.push(component);
        component.setParent(this);
    }

    public removeComponent(component: Component): void {
        const componentIndex = this.children.indexOf(component);
        this.children.splice(componentIndex, 1);

        component.setParent(null);
    }

    public isNested(): boolean {
        return true;
    }

    public operation(): string {
        const results = [];
        for (const child of this.children) {
            results.push(child.operation());
        }

        return `Branch(${results.join('+')})`;
    }
}

function clientCode(component: Component) {
    console.log(`RESULT: ${component.operation()}`);
}

const simple = new Product();
console.log('Client: I\'ve got a simple component:');
clientCode(simple);
console.log('');

const tree = new Box();

const box1 = new Box();
box1.addComponent(new Product());
box1.addComponent(new Product());

const box2 = new Box();
box2.addComponent(new Product());

tree.addComponent(box1);
tree.addComponent(box2);

clientCode(tree);
console.log('');

function clientCode2(component1: Component, component2: Component) {
    if (component1.isNested()) {
        component1.addComponent(component2);
    }
    console.log(`RESULT: ${component1.operation()}`);
}
clientCode2(tree, simple);