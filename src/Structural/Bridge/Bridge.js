class AbstractWriter {
  constructor(realizaciya) {
    this.realizaciya = realizaciya;
  }

  write() {
    return `write ${this.realizaciya.writeText()}`;
  }
}

class EEEEAbstractWriter extends AbstractWriter {
  write() {
    return `write ${this.realizaciya.writeText()} +  GREEN COLOR`;
  }
}

class Realiz {
  writeText() {
    return `+++`;
  }
}

class RealizIOS extends Realiz {
  writeText() {
    return `+++ IOS`;
  }
}

class RealizANDROID extends Realiz {
  writeText() {
    return `+++ ANDROID`;
  }
}

function clientCode1(abstractionWriter1) {
  console.log(abstractionWriter1.write());
}


let abstractionWriter1 = new AbstractWriter(new RealizIOS());
console.log("new AbstractWriter(new RealizIOS())");
clientCode1(abstractionWriter1);

console.log();


abstractionWriter1 = new EEEEAbstractWriter(new RealizIOS());
console.log("new EEEEAbstractWriter(new RealizIOS())");
clientCode1(abstractionWriter1);

console.log();


abstractionWriter1 = new EEEEAbstractWriter(new RealizANDROID());
console.log("new EEEEAbstractWriter(new  RealizANDROID())");
clientCode1(abstractionWriter1);

console.log();