class AbstractionWriter {
  protected realizaciya: Realizaciya;

  constructor(realizaciya: Realizaciya) {
    this.realizaciya = realizaciya;
  }

  public write(): string {
    const result = this.realizaciya.writeText();
    return `AbstractionWriter: write ${result}`;
  }
}

class Extended_AbstractionWriter extends AbstractionWriter {
  public write(): string {
    const result = this.realizaciya.writeText();
    return `Extended_AbstractionWriter: write ${result} + TTTTTTTT`;
  }
}

interface Realizaciya {
  writeText(): string;
}

class RealizaciyaIOS implements Realizaciya {
  public writeText(): string {
    return " IOS +++++++ fonts";
  }
}

class RealizaciyaAndroid implements Realizaciya {
  public writeText(): string {
    return " Android +++++++ fonts";
  }
}



function clientCode(abstractionWriter: AbstractionWriter) {
  console.log(abstractionWriter.write());
}

let abstractionWriter = new AbstractionWriter(new RealizaciyaIOS());
console.log('new AbstractionWriter(new RealizaciyaIOS())')
clientCode(abstractionWriter);

console.log()

console.log('new Extended_AbstractionWriter(new RealizaciyaAndroid())')
abstractionWriter = new Extended_AbstractionWriter(new RealizaciyaAndroid());
clientCode(abstractionWriter);

console.log()

console.log('new Extended_AbstractionWriter(new RealizaciyaIOS())')
abstractionWriter = new Extended_AbstractionWriter(new RealizaciyaIOS());
clientCode(abstractionWriter);
