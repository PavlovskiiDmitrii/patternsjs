interface DB {
  request(): void;
}

class RealDB implements DB {
  public request(): void {
    console.log("RealDB request.");
  }
}

class Proxyy implements DB {
  private realDB: RealDB;

  constructor(realDB: RealDB) {
    this.realDB = realDB;
  }

  public request(): void {
    console.log("ProxyyDB check.");
    if (this.checkAccess()) {
      this.realDB.request();
      this.cache();
    }
  }

  private checkAccess(): boolean {
    console.log("Proxyy: auth  Access.");
    return true;
  }

  private cache(): void {
    console.log("Proxyy: Logging the time of request.");
  }
}

function clientCode(subject: DB) {
  subject.request();
}

console.log("Client: realDB  new RealDB()");
clientCode(new RealDB());
console.log("");
console.log("Client: proxyDB new Proxyy(new RealDB())");
clientCode(new Proxyy(new RealDB()));
