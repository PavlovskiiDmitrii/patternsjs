class Russian {
    seyHello() {
        return 'Привет'
    }
}

class England {
    seyHello() {
        return 'Hello'
    }
}

function translit (word) {
    if (word) {
        return 'Привет';
    }
}

class AdapterNew extends Russian {
    constructor(england) {
        super();
        this.england = england;
    }

    seyHello() {
        const result = translit(this.england.seyHello);
        return result;
    }
}

function seyRusianHellow(target) {
    console.log(target.seyHello());
}


console.log('new Russian()');
seyRusianHellow(new Russian());


console.log('new England()');
seyRusianHellow(new England());


console.log('new AdapterNew(new England)');
seyRusianHellow(new AdapterNew(new England()));