class TargetA {
    public sentXML(): string {
        return 'TargetA = it is XML data';
    }
}

class TargetB {
    public LMXtnes(): string {
        return 'atad NOSJ si ti';
    }
}

class Adapter extends TargetA {
    private targetB: TargetB;

    constructor(targetB: TargetB) {
        super();
        this.targetB = targetB;
    }

    public sentXML(): string {
        const result = this.targetB.LMXtnes().split('').reverse().join('');
        return `Adapter: (TRANSLATED) ${result}`;
    }
}

function clientCode(target: TargetA) {
    console.log(target.sentXML());
}

console.log('new TargetA()');
clientCode(new TargetA());

console.log('');

const targetB = new TargetB();

console.log('targetB.LMXtnes()');
console.log(`TargetB: ${targetB.LMXtnes()}`);

console.log('');

console.log('new Adapter(targetB)');
clientCode(new Adapter(targetB));