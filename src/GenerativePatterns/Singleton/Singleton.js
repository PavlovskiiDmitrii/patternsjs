class SingltAn {
    static instance;
    static count = 0;

    static constructor () {}

    static getInstance() {
        if (!SingltAn.instance) {
            SingltAn.instance = new SingltAn();
        }

        return SingltAn.instance;
    }

    foo() {
        SingltAn.count++;
        console.log(`I am Singleton # ${SingltAn.count}`);
    }
}

function clientCode1() {
  const s1 = SingltAn.getInstance();
  const s2 = SingltAn.getInstance();

  s1.foo();
  s2.foo();
}

clientCode1();
