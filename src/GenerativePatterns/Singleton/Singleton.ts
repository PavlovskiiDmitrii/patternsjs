class Singleton {
  private static instance: Singleton;
  private count: number = 0;

  private constructor() {}

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }

  public foo() {
    this.count++;
    console.log(`I am Singleton # ${this.count}`);
  }
}

function clientCode() {
  const s1 = Singleton.getInstance();
  const s2 = Singleton.getInstance();

  s1.foo();
  s2.foo();
}

clientCode();
