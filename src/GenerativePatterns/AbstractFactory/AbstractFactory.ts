interface AbstractFactory {
  createCar(): Car;

  createShip(): Ship;
}
class FactoryKia implements AbstractFactory {
  public createCar(): Car {
    return new CarKia();
  }

  public createShip(): Ship {
    return new ShipKia();
  }
}
class FactoryMersedes implements AbstractFactory {
  public createCar(): Car {
    return new CarMersedes();
  }

  public createShip(): Ship {
    return new ShipMersedes();
  }
}

interface Car {
  exhaust(): string;
}
class CarKia implements Car {
  public exhaust(): string {
    return "exhaust Kia - pur pur pur";
  }
}
class CarMersedes implements Car {
  public exhaust(): string {
    return "exhaust Mersedes - ra ta ta ta ";
  }
}

interface Ship {
  displacement(): string;

  anotherUsefulFunctionB(collaborator: Car): string;
}
class ShipKia implements Ship {
  public displacement(): string {
    return "KIa displacement = 1000";
  }

  public anotherUsefulFunctionB(collaborator: Car): string {
    const result = collaborator.exhaust();
    return `The result of the B1 collaborating with the (${result})`;
  }
}
class ShipMersedes implements Ship {
  public displacement(): string {
    return "Mersedes displacement = 233";
  }

  public anotherUsefulFunctionB(collaborator: Car): string {
    const result = collaborator.exhaust();
    return `The result of the B2 collaborating with the (${result})`;
  }
}


function clientCode(factory: AbstractFactory) {
  const Car = factory.createCar();
  const Ship = factory.createShip();
  console.log(Ship.displacement());
  console.log(Ship.anotherUsefulFunctionB(Car));
}
clientCode(new FactoryMersedes());

// console.log("");

// console.log(
//   "Client: Testing the same client code with the second factory type..."
// );
// clientCode(new FactoryMersedes());
