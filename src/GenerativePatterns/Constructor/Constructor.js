class Car {
  constructor() {
    this.parts = [];
  }

  listParts() {
    console.log(`Product parts: ${this.parts}\n`);
  }
}

class Builder1 {
    constructor() {
        this.createNewProduct();
    }

    createNewProduct() {
        this.product = new Car();
    }

    createA() {
        this.product.parts.push('createA PartA1');
    }

    createB() {
        this.product.parts.push('createB PartB1');
    }

    getProduct() {
        const result = this.product;
        this.createNewProduct();
        return result;
    }
}

class Director1 {
  constructor() {}

  setBuilder(builder) {
    this.builder = builder;
  }

  buildMinimalViableProduct() {
    this.builder.createA();
  }

  buildFullFeaturedProduct() {
    this.builder.createA();
    this.builder.createB();
  }
}

clientCode = (director) => {
    const builder = new Builder1();
    director.setBuilder(builder);

    console.log('Build Minimal Viable Product:');
    director.buildMinimalViableProduct();
    // Возращаем рузельтат из строителя
    builder.getProduct().listParts();
}
const director1 = new Director1();
clientCode(director1);