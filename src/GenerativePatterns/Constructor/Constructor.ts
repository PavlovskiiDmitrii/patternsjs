interface Builder {
    createA(): void;
    createB(): void;
    createC(): void;
}

class ConcreteBuilder1 implements Builder {
    private product: Book;
    
    // Новый экземпляр строителя должен содержать пустой объект продукта
    constructor() {
        this.createNewProduct();
    }

    public createNewProduct(): void {
        this.product = new Book();
    }

    /**
     * Все этапы производства работают с одним и тем же экземпляром продукта.
     */
    public createA(): void {
        this.product.parts.push('createA PartA1');
    }

    public createB(): void {
        this.product.parts.push('createB PartB1');
    }

    public createC(): void {
        this.product.parts.push('createC PartC1');
    }

    /**
     * this.createNewProduct() на 41 строке не обязателен, просто он сбрасывает старый продукт, для следующего вызова
     * (можно это делать при явном вызове на сброс)
    */
    public getProduct(): Book {
        const result = this.product;
        this.createNewProduct();
        return result;
    }
}
class Book {
    public parts: string[] = [];

    public listParts(): void {
        console.log(`Product parts: ${this.parts}\n`);
    }
}
class Director {
    private builder: Builder;

    public setBuilder(builder: Builder): void {
        this.builder = builder;
    }

    // Директор может строить несколько вариаций продукта
    public buildMinimalViableProduct(): void {
        this.builder.createA();
    }

    public buildFullFeaturedProduct(): void {
        this.builder.createA();
        this.builder.createB();
        this.builder.createC();
    }
}

function clientCode(director: Director) {
    // создаём объект-строитель, и передаём его Директору
    const builder = new ConcreteBuilder1();
    director.setBuilder(builder);

    console.log('Build Minimal Viable Product:');
    director.buildMinimalViableProduct();
    // Возращаем рузельтат из строителя
    builder.getProduct().listParts();

    console.log('Build Full Featured Product:');
    director.buildFullFeaturedProduct();
    // Возращаем рузельтат из строителя
    builder.getProduct().listParts();

    // можно использовать без класса Директор.
    console.log('Custom product without class Director:');
    builder.createA();
    builder.createC();
    builder.getProduct().listParts();
}

const director = new Director();
clientCode(director);