abstract class Creator {

    public abstract factoryMethod(): Product;

    public someOperation(): string {
        const product = this.factoryMethod();
        return `был сделан продукт - ${product.getName()}`;
    }
}

class Creator1 extends Creator {
    public factoryMethod(): Product {
        console.log('Реализация factoryMethod для продукта 1')
        return new Product1();
    }
}

class Сreator2 extends Creator {
    public factoryMethod(): Product {
        console.log('Реализация factoryMethod для продукта 2')
        return new Product2();
    }
}


interface Product {
    getName(): string;
}

class Product1 implements Product {
    public getName(): string {
        return 'ПРОДУКТ 1';
    }
}

class Product2 implements Product {
    public getName(): string {
        return 'ПРОДУКТ 2';
    }
}

function createProduct(creator: Creator) {
    console.log(`Создан Кем? им - `, creator)
    console.log(creator.someOperation());
}

createProduct(new Creator1());
createProduct(new Сreator2());