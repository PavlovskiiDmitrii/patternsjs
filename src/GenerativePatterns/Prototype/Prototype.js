class TestCopyClass {
  constructor() {
    this.primitive = null;
    this.component = {};
  }

  clone() {
    const clone = Object.create(this);
    clone.primitive = this.primitive;
    clone.component = this.component;

    return clone;
  }
}

clientCode = () => {
  const A = new TestCopyClass();

  A.primitive = 44;
  A.component = { a: 10 };

  const B = A.clone();

  console.log('A', A)
  console.log('B', B)
};

clientCode();
